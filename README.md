# MyWorkstation: Bash Scripts and Ansible Playbooks to configure a DevOps VM

This project contains bash scripts and Ansible Playbooks to configure an Ubuntu GUEST Development machine. (HOST Config to come TODO:).

Saves several hours effort every time one of these needs rebuiding. 

## Pre-requisites
The pre-requisites to create a GUEST are:
- a **Linux** Host wtih Virtualbox 6.1 or later installed.

## i) On the HOST

### Step 1 - Basic Set-Up - accesses gitlab public repo
```
sudo apt install git # If required

git clone https://gitlab.com/Moortec-IaC/bashscripts/gitlab-ssh-setup.git

```

### Step 2 Create the GUEST Linux Virtual Machine
The script below specifies all the attributes of the VM, sets up the shared folders with the HOST, enables the clipboard, ...
```
cd myworkstation
$ chmod u+x ./make_guest.sh
```
make_guest.sh uses $1 for your VM name, $2 for local folders, and $3 for the ISO. For example...
```
$ ./make_guest.sh "DebianBookwormDev-3vCPU-6GB" "/mnt/Virtuals/Main" "/home/peter/Documents/Kits_and_Manuals/debian/debian-12.8.0-amd64-netinst.iso"

TODO: p4 the name of the Shared Folder as seen by the guest, default to /media/sf_HOST-share/ 
      p5 the HOSTS folder to be shared, currently I've been using two: /mnt/Virtuals/vm_projects and /home/peter/Virtual-machine-shr

```

### Step 3 Answer the prompts when the VM starts
Username, password, etc. 

This step takes about 30 minutes. 


### Step 4 Connect to the GUEST using the VirtualBox GUI

## On the GUEST

### Step 5 Post Installation script
This installs the Guest Additions and completes the set-up of the shared folder.
- Make the `guest_post_install.sh` bash script available to the GUEST; just cut-and-paste is probably easiest.
- Run the script `./guest_post_install.sh` Enter Sudo password when prompted.
- `sudo reboot`

This step takes a couple of minutes. 

### Step 6 - Complete the installation of the packages, apply config, etc.

- `git clone https://gitlab.com/moortec-dev/myworkstation`
- `cd myworkstation/GUEST`
- `chmod u+x ./a1_run_everything.sh`
- `./a1_run_everything.sh`
  
The full playbook takes about 30 mins to run. 

Or run individual playbooks 

```
GUEST$ ansible-playbook dev_workstation_main_playbook.yml --ask-become-pass
GUEST$ ansible-playbook playbooks/config/gitlab.yml --ask-become-pass
GUEST$ ansible-playbook playbooks/config/terraform.yml --ask-become-pass
GUEST$ ansible-playbook playbooks/config/bash.yml
```

### Step 7 - Complete the installation of the packages, apply config, etc.
