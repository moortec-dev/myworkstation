#!/usr/bin/env bash
# shellcheck disable=SC2155
#++
#  FACILITY:
#
#    Home Assistant Dynamic DNS
#
#  MODULE DESCRIPTION:
#
#      Updates node's AWS R53 A record to reflect changes made by an ISP. 
#
#  AUTHORS:
#
#      Peter Burke.
#
#  CREATION DATE:
#
#      13-APR-2021
#
#  ENVIRONMENT:
#
#      Compiler/Language:  Bash Script
#      Target system:      GNU bash, version 5.2.21(1)-release (x86_64-pc-linux-gnu)
#      Comments:           Should work in any bash environment.
#
#  DESIGN NOTES:
#
#      Creates a VirtualBox VM using the CLI. 
#      $1 is the name of the VM. $2 is the folder it should be created in. $3 is the ISO 
#
#
# TEST STATUS AND HISTORY:
#
#    Tested against GNU bash, version 5.2.21(1)-release (x86_64-pc-linux-gnu)
#
#  Version     Date        Performed by
#  -------     ----        ------------
#
#  0.1.0       20-DEC-24   Peter K. Burke
#
#  Partially tested.
# FIXME: specifiy the mnt point for the share as /mnt/HOST-share. 
#
#  MODIFICATION HISTORY (CHANGE LOG):
#
#  Version     Date        Author          Change request/review
#  -------     ----        ------          ---------------------
#
readonly VERSION=0.1.0 # Semantic versioning, MAJOR.MINOR.PATCH, https://semver.org/
#  0.1.0       20-DEC-24  Peter K. Burke   Initial version with this header and the full script wrap added.
#
# --
#
readonly SUBJECT=$(basename "$0") # Defaults to $0 (scriptname), or add another meaningful script name. basename strips the leading "./"
# Choose a location for the log file, if not using /tmp need to manage/rotate older logfiles.
# See this link if co-locating with the script: https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
readonly LOG_DIR="/tmp"  
readonly LOCK_FILE="$LOG_DIR/$SUBJECT.lock"                                 # Used to prevent multiple instances of script running concurrently.
readonly LOG_FILE="$LOG_DIR/$SUBJECT-$(date +"%Y-%m-%d-%H%M")-$VERSION.log" # Default log file name, includes a timestamp.

# Colour codes for output messages.
GREEN='\033[0;32m'
RED='\033[0;31m'
NOCOLOUR='\033[0;0m'

# #############################################################################
# Redirect stdout/err to control output messages.
# #############################################################################
# Copy stdout (FD1) and stderr (FD2), to FD3 and FD4 respectively, so can still output to the screen.
# Throw away FD1 - not interested in general command output. 
# Rediret FD2 to the log file, so normal messages for stderr are directed to the logfile. 
exec 3>&1 4>&2 1>/dev/null 2>"$LOG_FILE"
# Define echo functions 
echo_stdout() { echo -e "${GREEN} $SUBJECT>>> " "$@" "${NOCOLOUR}" >&3; }                                 # stdout to screen only,  unless script caller redirects on command line
echo_stderr() { echo -e "${RED} $SUBJECT>>> " "$@" "${NOCOLOUR}" >&4; }                                   # stderr to screen only,  unless script caller redirects on command line
echo_log()    { echo "$SUBJECT>>> " "$@" >&2; }                                                           # stderr to logfile only, (via FD2)
echo_outlog() { echo -e "${GREEN} $SUBJECT>" "$@" "${NOCOLOUR}" >&3; echo "$SUBJECT/stdout> " "$@" >&2; } # stdout to screen and logfile
echo_errlog() { echo -e "${RED} $SUBJECT>" "$@" "${NOCOLOUR}" >&4; echo "$SUBJECT/stderr> " "$@" >&2; }   # stderr to screen and logfile

# #############################################################################
# EDIT for Informative Tracing / Debugging
# #############################################################################
# uncomment a PS4= and the set -x command for tracing information.
#set -x 
export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#export PS4='\nDEBUG level:$SHLVL subshell-level: $BASH_SUBSHELL \nsource-file:${BASH_SOURCE} line#:${LINENO} function:${FUNCNAME[0]:+${FUNCNAME[0]}(): }\nstatement: '
#export PS4="# \e[38;5;239m(xtrace)\e[0m \e[4m\e[90m\$(basename \"\${BASH_SOURCE}\")\e[39m@\e[3mline\e[1m[\${LINENO}]\e[24m \e[1;34m>\e[0m \e[38;5;105m\${FUNCNAME[0]}()\e[0m: "

# #############################################################################
# Set Error Handling
# #############################################################################
# By default a shell script will plough on ignoring errors. But this not likely to be the desired behaviour. The set command makes the script behave more like a conventional programming language.
# -E [TLDR Cascade error handling to subshells.] Means error traps are inherited by shell functions, command substutions and subshells. Default is not to inherit
# -e [TLDR exit on error.] Exit immediatly if a command fails with a non-zero return status, unless the error is being handled. 
# -u [TLDR error/exit on undeclared vars] Error when trying to access unset vars, except special vars.
# -o pipefail [error if any pipe command fails] Ensures that if any command in a pipe sequence fails, the error is returned. The default is to use only the return code of the last command.
# set -Eeuo pipefail
# FIXME: the statement above is causing this script to crash out. Or its the ERR trap below?

# The trap command also improves error handling. The syntax is trap <function> <signals...> 
# Meaning that x_cleanup() will be called in response to the relevant signal (trap -l) EXIT, ERROR, or DEBUG
trap exit_cleanup EXIT            # Normal Exit
trap sig_cleanup SIGINT SIGTERM   # SIGINT is Ctrl+C, or user cancellation, SIGTERM is normal kill signal.
# trap err_cleanup ERR              # Error Exit

# #############################################################################
# Functions
# #############################################################################

# Trap functions
# shellcheck disable=SC2317  # Putting these after the shebang makes the scope the whole file.
all_cleanup() {
  
  if ! rm -f "$LOCK_FILE"; then
      echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')] $LOCK_FILE removal failed rm return code: $?:" >&2
      exit 1
  fi
  return 0
}

# shellcheck disable=SC2317  # Putting these after the shebang makes the scope the whole file.
exit_cleanup() {
  trap - SIGINT SIGTERM ERR EXIT # Reenable the default behaviour within the cleanup function.
  echo_outlog "exit_cleanup()"

  exec 2>&4 1>&3 # reset for errors in this function

  # script cleanup here
  all_cleanup 
  return $?
}

# shellcheck disable=SC2317  # Putting these after the shebang makes the scope the whole file.
sig_cleanup() {
  local lc="$BASH_COMMAND" rc=$?
  trap - SIGINT SIGTERM ERR EXIT # Reenable the default behaviour within the cleanup function. 
echo_errlog "sig_cleanup(): Script Interrupted, exiting"
  echo_errlog "Last command was [$lc]"

  exec 2>&4 1>&3 # reset for errors in this function

  # script cleanup here

  all_cleanup
  exit 1
}

# shellcheck disable=SC2317  # Putting these after the shebang makes the scope the whole file.
err_cleanup() {
  local lc="$BASH_COMMAND" rc=$?
  trap - SIGINT SIGTERM ERR EXIT # Reenable the default behaviour within the cleanup function. 
  
  echo_errlog "err_cleanup() Script Failed, exiting"
  echo_errlog "Last command was [$lc] that exited with code [$rc]"

  exec 2>&4 1>&3 # reset for errors in this function
  # script cleanup here

  all_cleanup # common cleanup  tasks
  exit 1
}
# #############################################################################
# End Functions
# #############################################################################

# #############################################################################
# Script Body [main(),MAIN]
# #############################################################################
echo_outlog "$SUBJECT Script starting... log file is $LOG_FILE"

# Stop multiple instances running, EDIT this out if this is not required.
if [[ -f "$LOCK_FILE" ]]; then
    echo_errlog "$SUBJECT Script is already running, exiting. Manually remove $LOCK_FILE if this message persists, as it may be left over from a previous failed run."
    exit 1
fi
touch "$LOCK_FILE" # Create the lock file, to stop another instance. all_cleanup() removes.

# Check for command line arguments
echo_outlog "$# command line arguments"

if [ $# -ne 3 ]
then
  echo_errlog "ERROR: incorrect number of command line arguments."
  exit 1
fi

# TODO: validate the parameters
# FIXME: parameterise p4 getting the bloody folder names the wrong way around 
VMNAME=$1
VMFOLDER=$2
ISO=$3

# HOST_SF_PATH="/mnt/Virtuals/vm_projects"
HOST_SF_PATH="/home/peter/Virtual-machine-shr"
GUEST_SF_MNT_POINT="/media/sf_HOST-share/"

echo_outlog "Creating VM $VMNAME in folder $VMFOLDER using ISO $ISO"

echo_outlog "--- Creating the VM $VMNAME, if it doesn't already exist."
VBoxManage showvminfo "$VMNAME" &> /dev/null
if [ "$?" = "0" ]; then
    echo_outlog "VM already exists."
else
    echo_outlog VBoxManage createvm --name $VMNAME --ostype Ubuntu_64 --register --basefolder $VMFOLDER
                VBoxManage createvm --name $VMNAME --ostype Ubuntu_64 --register --basefolder $VMFOLDER
fi
echo_outlog ""

echo_outlog "--- Creating the Shared Folder"
echo_outlog VBoxManage sharedfolder add $VMNAME --name "HOST-share" -hostpath $HOST_SF_PATH -automount 
            VBoxManage sharedfolder add $VMNAME --name "HOST-share" -hostpath $HOST_SF_PATH -automount 

# VBoxManage sharedfolder add $VMNAME --name vm_projects -hostpath $HOST_SHAREDFOLDER1 -automount

echo_outlog "--- Configuring Networking for a simple NAT network."
echo_outlog VBoxManage modifyvm $VMNAME --nic1 nat
VBoxManage modifyvm $VMNAME --nic1 nat
echo_outlog ""

echo_outlog "--- Allocating Memory and CPUs, specifying graphics controller. "
echo_outlog VBoxManage modifyvm $VMNAME --memory 6182 --cpus 3 --graphicscontroller vmsvga --vram 256
VBoxManage modifyvm $VMNAME --memory 6182 --cpus 3 --graphicscontroller vmsvga --vram 256
echo_outlog ""

echo_outlog "--- Allocating Memory and CPUs, specifying graphics controller. "
echo_outlog VBoxManage modifyvm $VMNAME --memory 6182 --cpus 3 --graphicscontroller vmsvga --vram 256
VBoxManage modifyvm $VMNAME --memory 6182 --cpus 3 --graphicscontroller vmsvga --vram 256
echo_outlog ""

echo_outlog "--- Enable the clipboard, bidirectional"
echo_outlog VBoxManage modifyvm $VMNAME --clipboard bidirectional
VBoxManage modifyvm $VMNAME --clipboard bidirectional

echo_outlog "-- Creating the hard drive $VMFOLDER/$VMNAME.vdi, if it doesn't already exist."
VBoxManage showmediuminfo "$VMFOLDER/$VMNAME.vdi" &> /dev/null
if [ "$?" = "0" ]; then
    echo_outlog "HDD already exists."
else
    echo_outlog VBoxManage createhd --filename "$VMFOLDER/$VMNAME.vdi" --size 50000 --format vdi
    VBoxManage createhd --filename "$VMFOLDER/$VMNAME.vdi" --size 50000 --format vdi
fi
echo_outlog ""

echo_outlog "Adding a SATA Controller"
VBoxManage storagectl $VMNAME --name "SATA Controller" --add sata --controller IntelAhci 
echo_outlog ""

echo_outlog "Attaching HDD storage"
echo_outlog VBoxManage storageattach $VMNAME --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium $VMFOLDER/$VMNAME.vdi
VBoxManage storageattach $VMNAME --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium $VMFOLDER/$VMNAME.vdi
echo_outlog ""

echo_outlog "Attaching IDE Controller "
echo_outlog VBoxManage storagectl $VMNAME --name "IDE Controller" --add ide --controller PIIX4
VBoxManage storagectl $VMNAME --name "IDE Controller" --add ide --controller PIIX4
echo_outlog ""

echo_outlog "Attaching $ISO "
echo_outlog VBoxManage storageattach $VMNAME --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium $ISO
VBoxManage storageattach $VMNAME --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium $ISO
echo_outlog ""

echo_outlog "Starting $VMNAME. Connect to the machine to complete the installation."
VBoxManage startvm $VMNAME

exit

#
# The VBox unattended install idea doesn't seem to work properly. The Guest Additions don't installed properly, 
# and simple things like the user created not being in the sudoers file, ...
# I can probably work around, but I don't have the time right now 13-Apr-2021.
#

echo "Setting up for Unattended Installation"
# Based on: https://docs.oracle.com/en/virtualization/virtualbox/6.0/user/basic-unattended.html
set -x
VBoxManage unattended install $VMNAME \
--iso=$ISO \
--user=peter --full-user-name=peter --password=peter \
--country=UK --time-zone=UTC \
--install-additions \
--locale=en_GB --country=UK \
--script-template       /usr/lib/virtualbox/UnattendedTemplates/ubuntu_preseed.cfg \
--post-install-template /usr/lib/virtualbox/UnattendedTemplates/debian_postinstall.sh
#--hostname=peter.local.net \ # For some reason this doesn't work.git 
echo ""

echo "starting the VM $VMNAME"
VBoxManage startvm $VMNAME --type headless

# Finally, delete any log files over the specified age. Debian's default behavour seems to be to 
# only purge /tmp on reboot. Managing the files here seems simpler than using systemd-tmpfiles.
# As find is likely to have permission errors need to disable the trap and eE. 
trap - ERR
set +eE # searching /tmp will likeliy generate "permission denied" errors, so disable termination
find "$LOG_DIR" -name "mt-*.log" -type f -mtime +35 -delete # mt-* filters out any that aren't managed by this script.
echo_outlog "find result code $?"
set -eE
trap err_cleanup ERR              # Error Exit

echo_outlog "$SUBJECT Script sucessful, finishing..."

exit 0 # Sucess exit

# #############################################################################
# End of main body
# #############################################################################
