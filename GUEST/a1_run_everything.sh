#!/bin/bash
set -x
if (( $EUID == 0 )); then
   echo "This script must NOT be run as root/sudo" 
   exit 1
fi
sudo apt install -y ansible
# Install any ansible galaxy roles that are used.
ansible-galaxy info
ansible-galaxy list
ansible-galaxy install -r ./install-roles.yml
ansible-galaxy list
#
ansible-playbook dev_workstation_main_playbook.yml --ask-become-pass
 

