#!/usr/bin/env bash
# set -x
# TODO: Parameterise this, for the username, etc.
su 
/usr/sbin/usermod -aG sudo peter
reboot # to sudo t akes effect # FIXME: 

echo "--- Install the Guest Additions CD."
read -p "Press Enter to continue when you have done so: " </dev/tty

echo "--- Installing the Guest Additions Pre-Reqs."
sudo apt install gcc make perl 

# This is the folder on debian, but there is a permissions problem trying to run it. 20/12/24
# need to sh /media...
# sudo mount -o remount,exec,ro /media/cdrom0 # be default has noexec flag set
sudo sh /media/cdrom0/VBoxLinuxAdditions.run # sh needed as device mounted noexec by default.

echo "Set-up the user for the shared folder access group."
sudo usermod -aG vboxsf peter

echo "Reboot, for the shared folder access to take effect."
sudo reboot
