#!/bin/bash
set -x
# Install any ansible galaxy roles that are used.
ansible-galaxy info
ansible-galaxy list
ansible-galaxy install -r ./install-roles.yml
ansible-galaxy list
#
ansible-playbook host-top-level-playbook.yml --ask-become-pass
