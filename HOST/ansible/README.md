 ansible-galaxy install -r ./install-roles.yml
 ansible-playbook host-top-level-playbook.yml --ask-become-pass
 There is also a dependency upon ansible-jsonpatch, which is installed manually for now; think this might become a standard module sooner or later.

 If running this on an old instance of an OS, then check the version inforamtion for:
 - vagrant
 - virtualbox
